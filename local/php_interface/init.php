<?php

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\UserTable;

AddEventHandler('iblock', 'OnAfterIBlockElementAdd', 'OnIBlockElementAddHandler');

function OnIBlockElementAddHandler($arFields)
{
    if ($arFields["ID"] > 0) {
        $iblockProperties = IblockTable::getById($arFields["IBLOCK_ID"])->fetch();

        if (
            $iblockProperties['CODE'] == 'IBLOCK_TRANSACTIONS'
            && str_contains($iblockProperties['NAME'], 'Ручное пополнение')
        ) {
            $idCurrentUser = CurrentUser::get()->getId();

            if ($idCurrentUser) {
                $setField = 'UF_BALANCE';

                $dataCurrentUser = UserTable::getByPrimary(
                    $idCurrentUser,
                    [
                        'select' => [
                            $setField
                        ],
                    ])->fetchObject();

                $balance = $dataCurrentUser->get($setField);
                $newBalance = (int)$balance + 100;

                $fields = [
                    $setField => $newBalance
                ];

                $user = new CUser;
                $user->update($idCurrentUser, $fields);
            }
        };
    }
}

/** второе задание **/

class TestUser extends CUser
{
    public function authenticateUser(string $login, string $password): void
    { /* ... */ }

    public function registerUser(array $userData): void
    { /* ... */ }

    public function changeUserPassword(int $userId, string $newPassword): void
    { /* ... */ }

    public function getUserInfo(int $userId): int
    {
        return $userId;
    }
}

$testUser = new TestUser();

$test = $testUser->getById(1)->fetch();

$result = $testUser->getUserInfo($test['ID']);
